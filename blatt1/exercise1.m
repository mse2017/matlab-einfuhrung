clear variables;
% Define variables
h = 2.2;
v1 = 13.4;
v2 = 13.7;
v3 = 13.5;
v4 = 13.8;
g = 9.81;
% Calculate v0
v0 = mean([v1, v2, v3, v4]);
% Define angles
a = 0:90;
% Calc vxy0
vx0 = v0*cosd(a);
vy0 = v0*sind(a);
% Calc & plot fa
fa = (vx0/g).*(vy0+ sqrt(vy0.^2+2*g*h));
figure;
hold on;
plot(a, fa);
title("Aufgabe 1.3");
xlabel('Winkel');
ylabel("Wurfweite");
axis([0 90 0 25]);

% Max point

[maxx, maxy] = max(fa);
plot(maxy, maxx, "*g");

legend("f(a)", "Maximaler Ausschlag");