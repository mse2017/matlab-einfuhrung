clear variables;
%Define variables
x = 0:25;
h = 2.2;
v1 = 13.4;
v2 = 13.7;
v3 = 13.5;
v4 = 13.8;
g = 9.81;
% Calculate v0
v0 = mean([v1, v2, v3, v4]);
figure;
hold on;
legenda = strings(6, 0);
for a=40:45
    fax = -1.*((g.*(x.^2))./(2.*((v0.*cosd(a)).^2)))+ x.*tand(a)+h;
    plot(x, fax);
    legenda(a-39) = strcat("Graph for ", num2str(a), " degrees");
end
axis([0 25 0 8]);
legend(legenda);



