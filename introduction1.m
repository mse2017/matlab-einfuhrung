
clear variables;
% plot sin(x) + cos(2*x) in [0, 120]

%vector
x = 0:120;

% sin(x) und cos(2x), sqrt(), max(), mean()
sinx = sind(x);
cos2x = cosd(2*x);

fx = sinx + cos2x;

%plot
figure;
hold on;
plot(x, fx);
plot(x, sind(x));
plot(30, 0.5, 'g*');
xlabel('x');
ylabel('y');
title('Titel der Arbeit');
legend('f(x)', 'sin(x)','Punkt');
axis([0 60 0 1]);