clear variables;
laenge = 100;
breite = 200;
[umfang, flaeche] = calcUF(laenge, breite);
fprintf("Flaeche: %d, Umfang: %d\n", [flaeche, umfang]);


%functions are below this line

function [umfang, flaeche] = calcUF(laenge, breite)
    flaeche = laenge*breite;
    umfang = 2*(laenge+breite);
end