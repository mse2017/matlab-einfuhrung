clear variables;
vector = zeros([10 0]);
for i=1:20
    vector(i)=rand();
end
sum = 0;
counter = 0;
added = [];
while sum<3 && counter<20
    counter = counter+1;
    sum = sum+vector(counter);
    added(counter) = vector(counter);
end
fprintf("Index: %f Value: %f Added numbers: %s\n", [counter, sum, sprintf("%f ",added)]);
if sum<3
    error("Die Werte sind kleiner als 3!");
end
